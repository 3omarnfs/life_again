import 'package:flutter/material.dart';
import 'package:life_again/modules/pacient_interface.dart';
import 'package:life_again/widgets/splash_screen_containers.dart';
import 'package:life_again/widgets/alert_dialog.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(
              'Now you can back to life and practice daily life easily',
              style: TextStyle(
                  color: Color(
                    0xff8D8E92DE,
                  ),
                  fontSize: 16),
            ),
            Column(
              children: [
                InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => PatientInterface()));
                    },
                    child: SplashScreenContainers(
                        imgText: 'assets/images/Flat.png', text: 'Patient')),
                SizedBox(height: 60,),
                InkWell(
                  onTap: () {
                    showDialog(
                      context: context,
                      builder: (context) => AlerttDialog(),
                    );
                  },
                  child: SplashScreenContainers(
                      imgText: 'assets/images/old-man.png', text: 'Escort'),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
